# Дипломный практикум в YandexCloud

[Репозиторий дипломного проекта на GitLab](https://gitlab.com/kylikovnikita/devops_netology_diplom)
---
---
## Этапы выполнения:

### Регистрация доменного имени

Цель:

  - Получить возможность выписывать [TLS сертификаты](https://letsencrypt.org) для веб-сервера.

Ожидаемые результаты:

1. У вас есть доступ к личному кабинету на сайте регистратора.
2. Вы зарезистрировали домен и можете им управлять (редактировать dns записи в рамках этого домена).

```
Использовал готовый DNS адрес с работы, отредактировал записи на статический IP, зарезервированный на Yandex.Cloud

```
![dns](imgs/dimpom_1_1_dns.PNG)


### Создание инфраструктуры

Для начала необходимо подготовить инфраструктуру в YC при помощи [Terraform](https://www.terraform.io/).


Предварительная подготовка:

1. Создайте сервисный аккаунт, который будет в дальнейшем использоваться Terraform для работы с инфраструктурой с необходимыми и достаточными правами. Не стоит использовать права суперпользователя
```
Создал аккаунт с правами `editor`
```
![dns](imgs/dimpom_1_2_serviceakk.PNG)

2. Подготовьте [backend](https://www.terraform.io/docs/language/settings/backends/index.html) для Terraform:
  - Рекомендуемый вариант: [Terraform Cloud](https://app.terraform.io/)
  - Альтернативный вариант: S3 bucket в созданном YC аккаунте.

3. Настройте [workspaces](https://www.terraform.io/docs/language/state/workspaces.html)
  - Рекомендуемый вариант: создайте два workspace: *stage* и *prod*. В случае выбора этого варианта все последующие шаги должны учитывать факт существования нескольких workspace.
  - Альтернативный вариант: используйте один workspace, назвав его *stage*. Пожалуйста, не используйте workspace, создаваемый Terraform-ом по-умолчанию (*default*).


```
Использовал рекомендуемый вариант app.terraform.io с одним воркспейсом
```
![dns](imgs/dimpom_1_3_terraform.PNG)

4. Создайте VPC с подсетями в разных зонах доступности.
5. Убедитесь, что теперь вы можете выполнить команды `terraform destroy` и `terraform apply` без дополнительных ручных действий.
```
Инфраструктура создается и уничтожается по клику
```
![dns](imgs/dimpom_1_4_tf_destroy.PNG)

6. В случае использования [Terraform Cloud](https://app.terraform.io/) в качестве [backend](https://www.terraform.io/docs/language/settings/backends/index.html) убедитесь, что применение изменений успешно проходит, используя web-интерфейс Terraform cloud.

![dns](imgs/dimpom_1_5_tf_apply.PNG)


Цель:

1. Повсеместно применять IaaC подход при организации (эксплуатации) инфраструктуры.
2. Иметь возможность быстро создавать (а также удалять) виртуальные машины и сети. С целью экономии денег на вашем аккаунте в YandexCloud.

```
Использую скрипты для создания, уничтожения инфраструктуры `terraform` и `ansible`а
```
![dns](imgs/dimpom_1_6_scripts.PNG)

Ожидаемые результаты:

1. Terraform сконфигурирован и создание инфраструктуры посредством Terraform возможно без дополнительных ручных действий.
2. Полученная конфигурация инфраструктуры является предварительной, поэтому в ходе дальнейшего выполнения задания возможны изменения.

```text
Инфраструктура сконфигурирована, управляется, переделывается, хранится в репозитории
```

---
### Установка Nginx и LetsEncrypt

Необходимо разработать Ansible роль для установки Nginx и LetsEncrypt.

**Для получения LetsEncrypt сертификатов во время тестов своего кода пользуйтесь [тестовыми сертификатами](https://letsencrypt.org/docs/staging-environment/), так как количество запросов к боевым серверам LetsEncrypt [лимитировано](https://letsencrypt.org/docs/rate-limits/).**

Рекомендации:
  - Имя сервера: `you.domain`
  - Характеристики: 2vCPU, 2 RAM, External address (Public) и Internal address.

Цель:

1. Создать reverse proxy с поддержкой TLS для обеспечения безопасного доступа к веб-сервисам по HTTPS.

Ожидаемые результаты:

1. В вашей доменной зоне настроены все A-записи на внешний адрес этого сервера:
  - `https://www.you.domain` (WordPress)
  - `https://gitlab.you.domain` (Gitlab)
  - `https://grafana.you.domain` (Grafana)
  - `https://prometheus.you.domain` (Prometheus)
  - `https://alertmanager.you.domain` (Alert Manager)

![dns](imgs/dimpom_1_1_dns.PNG)


2. Настроены все upstream для выше указанных URL, куда они сейчас ведут на этом шаге не важно, позже вы их отредактируете и укажите верные значения.

```
Все микросервисы смотрят на свои серверы, через {% if %}
```

```jinja
...
{% for item in domains_nginx %}
server {
    listen 80;
    server_name {{ item }}.{{ domain_name }};
    location /.well-known/acme-challenge {
        root /var/www/letsencrypt;
        try_files $uri $uri/ =404;
    }
    location / {
        rewrite ^ https://{{ item }}.{{ domain_name }}$request_uri? permanent;
    }
}
{% endfor %}
...
```

3. В браузере можно открыть любой из этих URL и увидеть ответ сервера (502 Bad Gateway). На текущем этапе выполнение задания это нормально!

![dns](imgs/dimpom_2_1_502.PNG)

___
### Установка кластера MySQL

Необходимо разработать Ansible роль для установки кластера MySQL.

Рекомендации:
  - Имена серверов: `db01.you.domain` и `db02.you.domain`
  - Характеристики: 4vCPU, 4 RAM, Internal address.

Цель:

1. Получить отказоустойчивый кластер баз данных MySQL.

Ожидаемые результаты:

1. MySQL работает в режиме репликации Master/Slave.

```yaml
...
- name: Check replica replication status.
  mysql_replication:
    mode: getreplica
    login_user: "{{ mysql_replication_user.name }}"
    login_password: "{{ mysql_replication_user.password }}"
  ignore_errors: true
  register: replica
  when:
    - mysql_replication_role == 'replica'
    - (mysql_replication_primary | length) > 0
  tags: ['skip_ansible_galaxy']

- name: Check primary replication status.
  mysql_replication: mode=getprimary
  delegate_to: "{{ mysql_replication_primary }}"
  register: primary
  when:
    - (replica.Is_Replica is defined and not replica.Is_Replica) or (replica.Is_Replica is not defined and replica is failed)
    - mysql_replication_role == 'replica'
    - (mysql_replication_primary | length) > 0
  tags: [ 'skip_ansible_galaxy' ]
...
```

2. В кластере автоматически создаётся база данных c именем `wordpress`

```yaml
# Databases.
mysql_databases:
  - name: wordpress
    collation: utf8_general_ci
    encoding: utf8
    replicate: 1
```


3. В кластере автоматически создаётся пользователь `wordpress` с полными правами на базу `wordpress` и паролем `wordpress`.

```yaml
# Users.
mysql_users:
  - name: wordpress
    host: '%'
    password: wordpress
    priv: '*.*:ALL PRIVILEGES'

  - name: repuser
    password: repuser
    priv: '*.*:REPLICATION SLAVE,REPLICATION CLIENT'
```
___
### Установка WordPress

Необходимо разработать Ansible роль для установки WordPress.

Рекомендации:
  - Имя сервера: `app.you.domain`
  - Характеристики: 4vCPU, 4 RAM, Internal address.

```htl
resource "yandex_compute_instance" "app" {
  name     = "app"
  hostname = "app.${var.host_name}"
  zone     = var.yc_zone[0]
  resources {
    cores  = 4
    memory = 4
    core_fraction = 20
  }
  scheduling_policy {
    preemptible = true
  }
  boot_disk {
    initialize_params {
      image_id = var.image_ubuntu22
      type     = "network-hdd"
      size     = "10"
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.sub_1.id
    nat       = true
  }
  metadata = {
    user-data = "${file("./meta.txt")}"
    sensitive = true
  }
}
```

Цель:

1. Установить [WordPress](https://wordpress.org/download/). Это система управления содержимым сайта ([CMS](https://ru.wikipedia.org/wiki/Система_управления_содержимым)) с открытым исходным кодом.


Ожидаемые результаты:

1. Виртуальная машина на которой установлен WordPress и Nginx/Apache (на ваше усмотрение).

```
WP установлен на apache2
```
```yaml
- name: Install LAMP Packages and PHP Extensions
  apt: name={{ item }} update_cache=yes state=latest
  loop: '{{ apt_packgs }}'
  register: result
  until: result is succeeded
  retries: 30
  delay: 20

- name: Set up Apache VirtualHost
  template:
    src: templates/apache.conf.j2
    dest: /etc/apache2/sites-available/{{ http_conf }}
  notify: reload apache
```

2. В вашей доменной зоне настроена A-запись на внешний адрес reverse proxy:
    - `https://www.you.domain` (WordPress)

3. На сервере `you.domain` отредактирован upstream для выше указанного URL и он смотрит на виртуальную машину на которой установлен WordPress.
4. В браузере можно открыть URL `https://www.you.domain` и увидеть главную страницу WordPress.

```
Подключение к БД происходит автоматически, заполняю первоначальную конфигурацию и после логина попадаю на стартовую страницу
```
![dns](imgs/dimpom_3_1_wp_config.PNG)
![dns](imgs/dimpom_3_2_wp_page.PNG)

---
### Установка Gitlab CE и Gitlab Runner

Необходимо настроить CI/CD систему для автоматического развертывания приложения при изменении кода.

Рекомендации:
  - Имена серверов: `gitlab.you.domain` и `runner.you.domain`
  - Характеристики: 4vCPU, 4 RAM, Internal address.

Цель:
1. Построить pipeline доставки кода в среду эксплуатации, то есть настроить автоматический деплой на сервер `app.you.domain` при коммите в репозиторий с WordPress.

Подробнее об [Gitlab CI](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/)

Ожидаемый результат:

1. Интерфейс Gitlab доступен по https.
2. В вашей доменной зоне настроена A-запись на внешний адрес reverse proxy:
    - `https://gitlab.you.domain` (Gitlab)
3. На сервере `you.domain` отредактирован upstream для выше указанного URL и он смотрит на виртуальную машину на которой установлен Gitlab.

```
Автоматически регистрируется пользователь `kulikov`, создается репозиторий `wp_netology` и происходит инициализирующий коммит папки `wordpress`
```
![dns](imgs/dimpom_4_1_gitlab.PNG)

3. При любом коммите в репозиторий с WordPress и создании тега (например, v1.0.0) происходит деплой на виртуальную машину.

```
При пуше в репозиторий автоматически происходит деплой в прод (даже при инициализирующем коммите)
```
![dns](imgs/dimpom_4_2_cicd.PNG)
___
### Установка Prometheus, Alert Manager, Node Exporter и Grafana

Необходимо разработать Ansible роль для установки Prometheus, Alert Manager и Grafana.

Рекомендации:
  - Имя сервера: `monitoring.you.domain`
  - Характеристики: 4vCPU, 4 RAM, Internal address.

Цель:

1. Получение метрик со всей инфраструктуры.

Ожидаемые результаты:

1. Интерфейсы Prometheus, Alert Manager и Grafana доступены по https.
2. В вашей доменной зоне настроены A-записи на внешний адрес reverse proxy:
  - `https://grafana.you.domain` (Grafana)
  - `https://prometheus.you.domain` (Prometheus)
  - `https://alertmanager.you.domain` (Alert Manager)
3. На сервере `you.domain` отредактированы upstreams для выше указанных URL и они смотрят на виртуальную машину на которой установлены Prometheus, Alert Manager и Grafana.
4. На всех серверах установлен Node Exporter и его метрики доступны Prometheus.

```
Node_exporter установлен и запущен на всех серверах
```

```log
TASK [node_exporter : Systemctl node-exporter Start] *************************************************************************************
changed: [articstranger.store]
changed: [app.articstranger.store]
changed: [db01.articstranger.store]
changed: [db02.articstranger.store]
changed: [monitoring.articstranger.store]
changed: [gitlab.articstranger.store]
changed: [runner.articstranger.store]

RUNNING HANDLER [node_exporter : systemd reload] *****************************************************************************************
ok: [articstranger.store]
ok: [app.articstranger.store]
ok: [monitoring.articstranger.store]
ok: [db02.articstranger.store]
ok: [db01.articstranger.store]
ok: [gitlab.articstranger.store]
ok: [runner.articstranger.store]
```

5. У Alert Manager есть необходимый [набор правил](https://awesome-prometheus-alerts.grep.to/rules.html) для создания алертов.

```yaml
...
- alert: InstanceDown
  expr: up == 0
  for: 5m
  labels:
    severity: critical
  annotations:
    summary: "Instance {{ $labels.instance }} down"
    description: "{{ $labels.instance }} of job {{ $labels.job }} has been down for more than 5 minutes."

- alert: APIHighRequestLatency
  expr: api_http_request_latencies_second{quantile="0.5"} > 1
  for: 10m
  labels:
    severity: warning
  annotations:
    summary: "High request latency on {{ $labels.instance }}"
    description: "{{ $labels.instance }} has a median request latency above 1s (current value: {{ $value }}s)"
...
```

2. В Grafana есть дашборд отображающий метрики из Node Exporter по всем серверам.

```
Скришоты всех web интерфейсов
```

![dns](imgs/dimpom_5_1_prom.PNG)
![dns](imgs/dimpom_5_2_alert.PNG)
![dns](imgs/dimpom_5_3_grafana.PNG)

---
## Что необходимо для сдачи задания?

1. Репозиторий со всеми Terraform манифестами и готовность продемонстрировать создание всех ресурсов с нуля.
```
Все инстансы установлены успешно
```

![dns](imgs/dimpom_6_1_tf_ok.PNG)

2. Репозиторий со всеми Ansible ролями и готовность продемонстрировать установку всех сервисов с нуля.

```
Все роли отработали успешно
```

![dns](imgs/dimpom_6_2_ansible_ok.PNG)

3. Скриншоты веб-интерфейсов всех сервисов работающих по HTTPS на вашем доменном имени.
  - `https://www.you.domain` (WordPress)
  - `https://gitlab.you.domain` (Gitlab)
  - `https://grafana.you.domain` (Grafana)
  - `https://prometheus.you.domain` (Prometheus)
  - `https://alertmanager.you.domain` (Alert Manager)

```
Все web интерфейсы
```

![dns](imgs/dimpom_6_3_web_ok.PNG)

4. Все репозитории рекомендуется хранить на одном из ресурсов ([github.com](https://github.com) или [gitlab.com](https://gitlab.com)).

    - [Репозиторий дипломного проекта на GitLab](https://gitlab.com/kylikovnikita/devops_netology_diplom)
