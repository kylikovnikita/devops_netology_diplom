#! /usr/bin/bash
echo "============ Destroy infrastructure ============"
echo
cd terraform

terraform destroy --auto-approve

cd ..