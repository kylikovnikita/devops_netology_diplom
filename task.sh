#! /usr/bin/bash

echo "============ ansible/ ============"
cd ansible

ansible-playbook task.yml

cd ..
echo
echo "============ End ============"
echo