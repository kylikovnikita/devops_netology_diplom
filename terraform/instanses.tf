
resource "yandex_compute_instance" "db01" {
  name     = "db01"
  hostname = "db01.${var.host_name}"
  zone     = var.yc_zone[0]
  resources {
    cores  = 4
    memory = 4
    core_fraction = 20
  }
  scheduling_policy {
    preemptible = true
  }
  boot_disk {
    initialize_params {
      image_id = var.image_ubuntu22
      type     = "network-hdd"
      size     = "10"
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.sub_1.id
    nat       = true
  }
  metadata = {
    user-data = "${file("./meta.txt")}"
    sensitive = true
  }
}

resource "yandex_compute_instance" "db02" {
  name     = "db02"
  hostname = "db02.${var.host_name}"
  zone     = var.yc_zone[1]
  resources {
    cores  = 4
    memory = 4
    core_fraction = 20
  }
  scheduling_policy {
    preemptible = true
  }
  boot_disk {
    initialize_params {
      image_id = var.image_ubuntu22
      type     = "network-hdd"
      size     = "10"
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.sub_2.id
    nat       = true
  }
  metadata = {
    user-data = "${file("./meta.txt")}"
    sensitive = true
  }
}

resource "yandex_compute_instance" "app" {
  name     = "app"
  hostname = "app.${var.host_name}"
  zone     = var.yc_zone[0]
  resources {
    cores  = 4
    memory = 4
    core_fraction = 20
  }
  scheduling_policy {
    preemptible = true
  }
  boot_disk {
    initialize_params {
      image_id = var.image_ubuntu22
      type     = "network-hdd"
      size     = "10"
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.sub_1.id
    nat       = true
  }
  metadata = {
    user-data = "${file("./meta.txt")}"
    sensitive = true
  }
}

resource "yandex_compute_instance" "gitlab" {
  name     = "gitlab"
  hostname = "gitlab.${var.host_name}"
  zone     = var.yc_zone[1]
  resources {
    cores  = 4
    memory = 8
    core_fraction = 20
  }
  scheduling_policy {
    preemptible = true
  }
  boot_disk {
    initialize_params {
      image_id = var.image_ubuntu22
      type     = "network-hdd"
      size     = "10"
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.sub_2.id
    nat       = true
  }
  metadata = {
    user-data = "${file("./meta.txt")}"
    sensitive = true
  }
}

resource "yandex_compute_instance" "runner" {
  name     = "runner"
  hostname = "runner.${var.host_name}"
  zone     = var.yc_zone[0]
  resources {
    cores  = 4
    memory = 4
    core_fraction = 20
  }
  scheduling_policy {
    preemptible = true
  }
  boot_disk {
    initialize_params {
      image_id = var.image_ubuntu22
      type     = "network-hdd"
      size     = "10"
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.sub_1.id
    nat       = true
  }
  metadata = {
    user-data = "${file("./meta.txt")}"
    sensitive = true
  }
}

resource "yandex_compute_instance" "monitoring" {
  name     = "monitoring"
  hostname = "monitoring.${var.host_name}"
  zone     = var.yc_zone[1]
  resources {
    cores  = 4
    memory = 4
    core_fraction = 20
  }
  scheduling_policy {
    preemptible = true
  }
  boot_disk {
    initialize_params {
      image_id = var.image_ubuntu22
      type     = "network-hdd"
      size     = "10"
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.sub_2.id
    nat       = true
  }
  metadata = {
    user-data = "${file("./meta.txt")}"
    sensitive = true
  }
}
