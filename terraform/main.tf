terraform {
  backend "remote" {
    organization = "kylich"
    workspaces {
      name = "stage"
    }
  }

	required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
	}

  required_version = ">= 0.14.0"
}

provider "yandex" {
	token     = var.yc_token
	cloud_id  = var.yc_cloud_id
	folder_id = var.yc_folder_id
	zone      = var.yc_zone[0]
}

data "yandex_iam_service_account" "service_akk" {
  service_account_id = var.yc_service_id
}