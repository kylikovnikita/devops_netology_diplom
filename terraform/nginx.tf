resource "yandex_compute_instance" "nginx" {
  name     = "nginx"
  zone     = var.yc_zone[0]
  hostname = var.host_name

  resources {
    cores  = 2
    memory = 2
    core_fraction = 20
  }

  boot_disk {
    initialize_params {
      image_id = var.image_ubuntu22
      type     = "network-hdd"
      size     = "10"
    }
  }

  network_interface {
    subnet_id      = yandex_vpc_subnet.sub_1.id
    nat            = true
    nat_ip_address = var.nat_ip_address
  }

  metadata = {
    user-data = "${file("./meta.txt")}"
  }

}