resource "yandex_vpc_network" "netology" {
  name = "netology"
}

resource "yandex_vpc_subnet" "sub_1" {
  name           = "sub_1"
  zone           = var.yc_zone[0]
  network_id     = yandex_vpc_network.netology.id
  v4_cidr_blocks = [ "10.10.0.0/24" ]

  depends_on = [
    yandex_vpc_network.netology
  ]
}

resource "yandex_vpc_subnet" "sub_2" {
  name           = "sub_2"
  zone           = var.yc_zone[1]
  network_id     = yandex_vpc_network.netology.id
  v4_cidr_blocks = [ "10.20.0.0/24" ]

  depends_on = [
    yandex_vpc_network.netology
  ]
}
