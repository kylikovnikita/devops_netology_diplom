
variable "yc_token" {
  type      = string
  sensitive = true
}

variable "yc_cloud_id" {
  type      = string
  sensitive = false
}

variable "yc_folder_id" {
  type      = string
  sensitive = false
}

variable "yc_zone" {
  type      = list(string)
  sensitive = false
  default   = [
    "ru-central1-b",
    "ru-central1-a",
  ]
}

variable "yc_service_id" {
  type      = string
  sensitive = true
}

variable "host_name" {
  type      = string
  sensitive = false
  default   = "articstranger.store"
}

variable "nat_ip_address" {
  type      = string
  sensitive = false
  default   = "130.193.40.61"
}

variable "image_ubuntu22" {
  type      = string
  sensitive = false
  default   = "fd8uoiksr520scs811jl"
}