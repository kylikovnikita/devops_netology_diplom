#! /usr/bin/bash

echo "============ git pull ============"
git pull

echo "============ terraform/ ============"
cd terraform

echo "============ Copy id_rsa to meta.txt ============"
echo

if [ -e /home/kulikov/.ssh ]
then
    echo .ssh exist
else
    echo .ssh creating
    mkdir /home/kulikov/.ssh
fi

if [ -e /home/kulikov/.ssh/id_rsa ]
then
    echo id_rsa exist
else
    echo id_rsa creating
    ssh-keygen -t rsa -N '' -f /home/kulikov/.ssh/id_rsa <<< y
fi

sshvar=`sudo cat /home/kulikov/.ssh/id_rsa.pub`
sshuser=`echo $sshvar | awk '{print $NF}'`

if grep -q $sshuser meta.txt; then
    echo "id_rsa is alredy in meta.txt"
else
    echo "adding id_rsa to meta.txt"
    echo "      - $sshvar" >> meta.txt
fi

echo "============ Create infrastructure ============"
echo

terraform apply --auto-approve

cd ..
echo "============ ansible/ ============"
cd ansible

echo
echo "============ Ping-Pong ============"
echo

s=10
for (( i=100; i>0; i=i-$s ))
do
    echo "Waiting $i secs..."
    sleep $s
done

chmod 600 ~/.ssh/id_rsa
ssh-keygen -R articstranger.store
ansible nginx -m ping
scp ~/.ssh/id_rsa kulikov@articstranger.store:/home/kulikov/.ssh/id_rsa && chmod 0600 ~/.ssh/id_rsa

echo
echo "============ Run Playbook ============"
echo

ansible-playbook playbook.yml

cd ..
echo
echo "============ End ============"
echo