---

- name: Create .gitlab-runner dir
  file:
    path: "{{ gitlab_runner_config_file_location }}"
    state: directory
    mode: 0700
  become: true

- name: Ensure config.toml exists
  file:
    path: "{{ gitlab_runner_config_file }}"
    state: touch
    modification_time: preserve
    access_time: preserve
  become: true

- name: Set concurrent option
  lineinfile:
    dest: "{{ gitlab_runner_config_file }}"
    regexp: '^(\s*)concurrent ='
    line: '\1concurrent = {{ gitlab_runner_concurrent }}'
    state: present
    backrefs: yes
  become: true
  notify:
    - restart_gitlab_runner

- name: Add listen_address to config
  lineinfile:
    dest: "{{ gitlab_runner_config_file }}"
    regexp: '^listen_address ='
    line: 'listen_address = "{{ gitlab_runner_listen_address }}"'
    insertafter: '\s*concurrent.*'
    state: present
  when: gitlab_runner_listen_address | length > 0  # Ensure value is set
  become: true
  notify:
    - restart_gitlab_runner

- name: Add log_format to config
  lineinfile:
    dest: "{{ gitlab_runner_config_file }}"
    regexp: '^log_format ='
    line: 'log_format = "{{ gitlab_runner_log_format|default("runner") }}"'
    insertbefore: BOF
    state: present
  when: gitlab_runner_log_format is defined # Ensure value is set
  become: true
  notify:
    - restart_gitlab_runner

- name: Add sentry dsn to config
  lineinfile:
    dest: "{{ gitlab_runner_config_file }}"
    regexp: '^sentry_dsn ='
    line: 'sentry_dsn = "{{ gitlab_runner_sentry_dsn }}"'
    insertafter: '\s*concurrent.*'
    state: present
  when: gitlab_runner_sentry_dsn | length > 0  # Ensure value is set
  become: true
  notify:
    - restart_gitlab_runner

- name: Add session server listen_address to config
  lineinfile:
    dest: "{{ gitlab_runner_config_file }}"
    regexp: '^(\s+)listen_address ='
    line: '  listen_address = "{{ gitlab_runner_session_server_listen_address }}"'
    insertafter: '^\s*\[session_server\]'
    state: "{{ 'present' if gitlab_runner_session_server_listen_address | length > 0 else 'absent' }}"
  become: true
  notify:
    - restart_gitlab_runner

- name: Add session server advertise_address to config
  lineinfile:
    dest: "{{ gitlab_runner_config_file }}"
    regexp: '^\s*advertise_address ='
    line: '  advertise_address = "{{ gitlab_runner_session_server_advertise_address }}"'
    insertafter: '^\s*\[session_server\]'
    state: "{{ 'present' if gitlab_runner_session_server_advertise_address | length > 0 else 'absent' }}"
  become: true
  notify:
    - restart_gitlab_runner

- name: Add session server session_timeout to config
  lineinfile:
    dest: "{{ gitlab_runner_config_file }}"
    regexp: '^\s*session_timeout ='
    line: "  session_timeout = {{ gitlab_runner_session_server_session_timeout }}"
    insertafter: '^\s*\[session_server\]'
    state: present
  when: gitlab_runner_session_server_session_timeout
  become: true
  notify:
    - restart_gitlab_runner
