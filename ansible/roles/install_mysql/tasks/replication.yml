---
- name: Ensure replication user exists on primary.
  mysql_user:
    name: "{{ mysql_replication_user.name }}"
    host: "{{ mysql_replication_user.host | default('%') }}"
    password: "{{ mysql_replication_user.password }}"
    priv: "{{ mysql_replication_user.priv | default('*.*:REPLICATION SLAVE,REPLICATION CLIENT') }}"
    state: present
  when:
    - mysql_replication_role == 'primary'
    - mysql_replication_user.name is defined
    - (mysql_replication_primary | length) > 0
  tags: ['skip_ansible_galaxy']

- name: Check replica replication status.
  mysql_replication:
    mode: getreplica
    login_user: "{{ mysql_replication_user.name }}"
    login_password: "{{ mysql_replication_user.password }}"
  ignore_errors: true
  register: replica
  when:
    - mysql_replication_role == 'replica'
    - (mysql_replication_primary | length) > 0
  tags: ['skip_ansible_galaxy']

- name: Check primary replication status.
  mysql_replication: mode=getprimary
  delegate_to: "{{ mysql_replication_primary }}"
  register: primary
  when:
    - (replica.Is_Replica is defined and not replica.Is_Replica) or (replica.Is_Replica is not defined and replica is failed)
    - mysql_replication_role == 'replica'
    - (mysql_replication_primary | length) > 0
  tags: [ 'skip_ansible_galaxy' ]

- name: Configure replication on the replica.
  mysql_replication:
    mode: changeprimary
    primary_host: "{{ mysql_replication_primary }}"
    primary_user: "{{ mysql_replication_user.name }}"
    primary_password: "{{ mysql_replication_user.password }}"
    primary_log_file: "{{ primary.File }}"
    primary_log_pos: "{{ primary.Position }}"
  ignore_errors: true
  when:
    - (replica.Is_Replica is defined and not replica.Is_Replica) or (replica.Is_Replica is not defined and replica is failed)
    - mysql_replication_role == 'replica'
    - mysql_replication_user.name is defined
    - (mysql_replication_primary | length) > 0

- name: Start replication.
  mysql_replication: mode=startreplica
  when:
    - (replica.Is_Replica is defined and not replica.Is_Replica) or (replica.Is_Replica is not defined and replica is failed)
    - mysql_replication_role == 'replica'
    - (mysql_replication_primary | length) > 0
  tags: ['skip_ansible_galaxy']
