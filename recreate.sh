#! /usr/bin/bash
echo "========= terraform/ ========="
cd terraform

echo "============ Destroy infrastructure ============"
echo

terraform destroy --auto-approve

cd ..
echo "============ Call ./create.sh ============"
echo

./create.sh