#! /usr/bin/bash

echo "============ ansible/ ============"
cd ansible

ansible-playbook playbook.yml

cd ..
echo
echo "============ End ============"
echo